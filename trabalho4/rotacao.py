#coding: utf-8

####################################################
## MC920 1s18 - Trabalho 4: Transformação Rotação ##
## Aluna: Elisa Dell'Arriva - ra135551            ##
####################################################


# BIBLIOTECAS
import numpy as np
import scipy as sp
import interpolacao as ip

#FUNÇÕES
def rotacionar(angulo_rot, interp, imgi, imgo):
	print("Transformação de Rotação, ângulo de ", angulo_rot, " graus.")
	for xo in range(0, imgo.shape[0]):
		for yo in range(0, imgo.shape[1]):
			v = 0.0

			xtmp = xo*np.cos(angulo_rot*3.14/180) + yo*np.sin(angulo_rot*3.14/180)
			ytmp = yo*np.cos(angulo_rot*3.14/180) - xo*np.sin(angulo_rot*3.14/180)

			v = ip.interpolar(imgi, xtmp, ytmp, interp)

			imgo[xo][yo] = v
			
	return imgo
		

#if __name__ == "__main__":
#    rotacionar()
    #TODO: adequar chamada para passa os parâmetros necessátios.
