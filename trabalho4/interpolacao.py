#coding: utf-8

##################################################
## MC920 1s18 - Trabalho 4: Interpolações       ##
## Aluna: Elisa Dell'Arriva - ra135551          ##
##################################################


# BIBLIOTECAS
import sys
import math
import numpy as np
import scipy as sp

#FUNÇÕES

#Interpolação por vizinho mais próximo
def vizinho_mais_proximo(xtmp, ytmp, imgi):
	#Arredonda coordenadas intermediárias com nenhum dígito decimal,
	# ou seja, para o inteiro mais próximo
	xi = int(round(xtmp, 0))
	yi = int(round(ytmp, 0))

	#Trata casos de borda
	if (xi >= imgi.shape[0] or yi >= imgi.shape[1] or xi < 0 or yi < 0):
		return 0

	return imgi[xi][yi]

#Interpolação bilinear
def bilinear(xtmp, ytmp, imgi):
	v = 0.0

	#Obtem xi, yi a partir das coordenadas intermediárias
	xi = math.floor(xtmp)
	yi = math.floor(ytmp)

	#Trata casos de borda
	if (xi >= (imgi.shape[0] - 1) or yi >= (imgi.shape[1] - 1) or xi < 0 or yi < 0):
		return 0

	#Calcula as distâncias entre xtmp e xi, e ytmp e yi
	dx = xtmp - xi
	dy = ytmp - yi

	v = (1 - dx)*(1 - dy)*imgi[xi][yi] + \
		dx*(1 - dy)*imgi[xi+1][yi] + \
		(1 - dx)*dy*imgi[xi][yi+1] + \
		dx*dy*imgi[xi+1][yi+1]

	return v

#Função potência cúbica
def potencia_cubica(x):
	r = 0.00

	if (x <= 0):
		r = x ** (3.00)
	else:
		r = -(-x) ** (3.00)

	return r

#Função R(s), auxiliar à interpolação bicúbica
def bicubica_R(s):
	#TODO
	r = 0.0

	r = (1/6) * ( \
		potencia_cubica(bicubica_P(s + 2)) - \
		4 * potencia_cubica(bicubica_P(s + 1)) + \
		6 * potencia_cubica(bicubica_P(s)) - \
		4 * potencia_cubica(bicubica_P(s - 1)) \
		)

	return r

#Função P(t), auxiliar à interpolação bicúbica
def bicubica_P(t):
	if (t <= 0):
		t = 0

	return t

#Interpolação bicúbica
def bicubica(xtmp, ytmp, imgi):
	v = 0.0

	#Obtem xi, yi a partir das coordenadas intermediárias
	xi = math.floor(xtmp)
	yi = math.floor(ytmp)

	#Trata casos de borda
	if (xi >= (imgi.shape[0] - 1) or yi >= (imgi.shape[1] - 1) or xi <= 0 or yi <= 0):
		return 0

	#Calcula as distâncias entre xtmp e xi, e ytmp e yi
	dx = xtmp - xi
	dy = ytmp - yi

	#Função da interpolação bicúbica
	for m in range(-1, 2):
		for n in range(-1, 2):
			v += imgi[xi + m][yi + n] * \
				 bicubica_R(m - dx) * \
				 bicubica_R(dy - n)

	return v

#Função L(n), auxiliar à interpolação por polinômios de Lagrange
def polinomios_lagrange_L(n, dx, dy, xi, yi, imgi):
	l = 0.0

	l = (1/6) * (-dx*(dx - 1)) * (dx - 2) * imgi[xi - 1][yi + n - 2] + \
		(1/2) * (dx + 1) * (dx - 1) * (dx - 2) * imgi[xi][yi + n - 2] + \
		(1/2) * (-dx*(dx + 1)) * (dx - 2) * imgi[xi + 1][yi + n - 2] + \
		(1/6) * (dx*(dx + 1)) * (dx - 1) * imgi[xi + 2][yi + n - 2]

	return l


#Interpolação por polinômios de Lagrange
def polinomios_lagrange(xtmp, ytmp, imgi):
	v = 0.0

	#Obtem xi, yi a partir das coordenadas intermediárias
	xi = math.floor(xtmp)
	yi = math.floor(ytmp)

	#Trata casos de borda
	if (xi >= (imgi.shape[0] - 2) or yi >= (imgi.shape[1] - 2) or xi <= 0 or yi <= 0):
		return 0

	#Calcula as distâncias entre xtmp e xi, e ytmp e yi
	dx = xtmp - xi
	dy = ytmp - yi

	#Função da interpolação por polinômios de Lagrange
	v = (1/6) * (-dy*(dy - 1)) * (dy - 2) * polinomios_lagrange_L(1, dx, dy, xi, yi, imgi) + \
		(1/2) * (dy + 1) * (dy - 1) * (dy- 2) * polinomios_lagrange_L(2, dx, dy, xi, yi, imgi) + \
		(1/2) * (-dy*(dy + 1)) * (dy - 2) * polinomios_lagrange_L(3, dx, dy, xi, yi, imgi) + \
		(1/6) * (dy*(dy + 1)) * (dy - 1) * polinomios_lagrange_L(4, dx, dy, xi, yi, imgi)

	return v

#função principal
def interpolar(imgi, xtmp, ytmp, metodo):
	v = 0.0

	if (metodo == 'vp'):
		#print("Interpolação por vizinho mais próximo.")
		v = vizinho_mais_proximo(xtmp, ytmp, imgi)
	
	elif (metodo == 'bl'):
		#print("Interpolação bilinear.")
		v = bilinear(xtmp, ytmp, imgi)
	
	elif (metodo == 'bc'):
		#print("Interpolação bicúbica.")
		v = bicubica(xtmp, ytmp, imgi)
	
	elif (metodo == 'pl'):
		#print("Interpolação por polinômios de Lagrange.")
		v = polinomios_lagrange(xtmp, ytmp, imgi)
	
	else:
		print("Método de interpolação inválido. Use: 'vp' para vizinho mais próximo, 'bl' para bilinear, 'bc' para bicúbica ou 'pl' para polinômios de lagrange")

	return v
		
if __name__ == "__main__":
    interpolar()

