#coding: utf-8

####################################################
## MC920 1s18 - Trabalho 4: Transformação Escala  ##
## Aluna: Elisa Dell'Arriva - ra135551            ##
####################################################


# BIBLIOTECAS
import numpy as np
import scipy as sp
import interpolacao as ip

#FUNÇÕES
def escalonar(fator_escala, interp, imgi, imgo):
	print("Transformação de escala, com fator de escala ", fator_escala, ".")
	
	for xo in range(0, imgo.shape[0]):
		xtmp = xo/fator_escala
		
		for yo in range(0, imgo.shape[1]):
			v = 0.0
			ytmp = yo/fator_escala

			v = ip.interpolar(imgi, xtmp, ytmp, interp)

			imgo[xo][yo] = v
			
	return imgo


#if __name__ == "__main__":
#    escalonar()
    #TODO: rever chamada, para mandar parâmetros!!!

