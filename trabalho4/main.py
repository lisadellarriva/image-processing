#coding: utf-8

####################################################
## MC920 1s18 - Trabalho 4                        ##
## Aluna: Elisa Dell'Arriva - ra135551            ##
####################################################


# BIBLIOTECAS
import sys
import math
import argparse
import escala
import rotacao
import redimensao
import numpy as np
import matplotlib.pyplot as plot
import matplotlib.image as mpimg

def main():
	#Faz o parseamento dos parâmetros de entrada
	parser = argparse.ArgumentParser(description="Argument parser of geometric transformations.")

	parser.add_argument('-a', '--angulo', type=float)
	parser.add_argument('-e', '--escala', type=float)
	parser.add_argument('-d', '--dimensao', type=float, nargs=2)
	parser.add_argument('-m', '--interpolacao', type=str)
	parser.add_argument('-i', '--imgentrada', type=str)
	parser.add_argument('-o', '--imgsaida', type=str)
	args = parser.parse_args()

	#Abre imagem de entrada
	imgi = mpimg.imread(args.imgentrada)

	#Seleciona transformação escolhida pelo usuário
	if (args.angulo == None and args.escala == None and args.dimensao == None):
		print("Nenhuma transformação geométrica escolhida. Use: -a para rotação ou -e para escala ou -d para redimensionamento.")
	
	elif (args.angulo != None and args.escala != None and args.dimensao != None):
		print("Você deve escolher APENAS uma das transformações geométricas: -a para rotação ou -e para escala ou -d para redimensionamento.")
	
	elif (args.angulo != None):
		#Cria imagem de saída com a mesma dimensão da imagem de entrada
		imgo = np.zeros((imgi.shape[0], imgi.shape[1]))
		
		#Chama função de roracao (caso um dos -d ou -e tenha valor, será ignorado!)
		imgfinal = rotacao.rotacionar(args.angulo, args.interpolacao, imgi, imgo)
	
	elif (args.escala != None):
		#Calcula dimensão da imagem de saída a partir do fator de escala
		dimx = math.floor(imgi.shape[0] * args.escala)
		dimy = math.floor(imgi.shape[1] * args.escala)

		#Cria imagem de saída com dimensão adequada para a escala
		imgo = np.zeros((dimx, dimy))

		#Chama função de escala
		imgfinal = escala.escalonar(args.escala, args.interpolacao, imgi, imgo)

	else:
		#Cria imagem de saída com dimensão requisitada
		imgo = np.zeros((int(args.dimensao[1]), int(args.dimensao[0])))

		#Chama função de redimensionar
		imgfinal = redimensao.redimensionar(args.dimensao, args.interpolacao, imgi, imgo)

	
	#Plota e salva imagem transformada
	plot.imshow(imgfinal, cmap = plot.get_cmap('gray'))
	plot.title("Imagem transformada")
	plot.show()
	mpimg.imsave(args.imgsaida, imgfinal)


if __name__ == "__main__":
    main()
