#coding: utf-8

#######################################################
## MC920 1s18 - Trabalho 4: Transformação Redimensão ##
## Aluna: Elisa Dell'Arriva - ra135551               ##
#######################################################


# BIBLIOTECAS
import numpy as np
import scipy as sp
import interpolacao as ip

#FUNÇÕES
def redimensionar(dimensao, interp, imgi, imgo):
	print("Transformação de Redimensionamento para ", dimensao[1], " x ", dimensao[0], ".")
	
	fatorx = dimensao[1]/imgi.shape[0]
	fatory = dimensao[0]/imgi.shape[1]

	for xo in range(0, imgo.shape[0]):
		xtmp = xo/fatorx
		
		for yo in range(0, imgo.shape[1]):
			v = 0.0
			ytmp = yo/fatory

			v = ip.interpolar(imgi, xtmp, ytmp, interp)

			imgo[xo][yo] = v
			
	return imgo


#if __name__ == "__main__":
 #   redimensionar()
    #TODO: rever chamada, para mandar parâmetros!!!

