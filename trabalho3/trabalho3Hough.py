#coding: utf-8

####################################################
## MC920 1s18 - Trabalho 3: Transformada de Hough ##
## Aluna: Elisa Dell'Arriva - ra135551            ##
####################################################


# BIBLIOTECAS
import sys
import numpy as np
import scipy as sp
import matplotlib.pyplot as plot
import matplotlib.image as mpimg
from skimage import filter as skif	#scikit-image 0.10.x
from skimage import color as skic
from skimage import morphology as skim
from skimage import segmentation as skis
from skimage import transform as skit

#FUNÇÕES
#faz a leitura da imagem
def leitura_imagem():
	img_entrada_name = sys.argv[1]
	img = mpimg.imread(img_entrada_name)
	img = skic.rgb2gray(img)	#transforma em escala de cinza

	return img

#realiza o pré processamento na imagem
def preprocessamento_hough(imgb):
	#aplicação do filtro canny
	imgb = skif.canny(imgb)

	#inverção e binarização da imagem
	maskF = imgb == False
	maskT = imgb == True
	imgb[maskF] = 1
	imgb[maskT] = 0

	return imgb

#função auxiliar ao algoritmo de projeção horizontal
def fc_objetivo(acc, angles, dist):
	indx = np.argmax(acc)
	d = dist[int(indx/acc.shape[1])]
	a = angles[indx % acc.shape[1]]
	max_angle = np.rad2deg(a)
	
	return max_angle

#realiza a projeção horizontal da imagem
def transformada_hough(imgb):
	intervalo_angle = np.ndarray(shape=(2), dtype=float)
	intervalo_angle[0] = np.deg2rad(0.0)
	intervalo_angle[1] = np.deg2rad(180.0)

	acc, angles, dist = skit.hough_line(imgb, intervalo_angle)
	#a, t, d = skit.hough_line_peaks(acc, angles, dist, min_distance=1, min_angle=3)
	
	max_angle = fc_objetivo(acc, angles, dist)

	return max_angle

#rotaciona imagem original
def rotacao_imagem(img, angle, out_path):
	imgr = skit.rotate(img, angle)	
	
	mpimg.imsave(out_path, imgr)	#salva imagem rotacionada
	plot.imshow(imgr, cmap = plot.get_cmap('gray'))
	plot.title("TH: Imagem original rotacionada")
	plot.show()

#função principal
def main_th():
	img = leitura_imagem()
	imgb = preprocessamento_hough(img)

	#plot da imagem após o pré-processamento
	plot.imshow(imgb, cmap = plot.get_cmap('gray'))
	plot.title("TH: Imagem após pré-processamento")
	plot.show()

	best_angle = transformada_hough(imgb)
	print("Ângulo: ", best_angle)
	rotacao_imagem(img, best_angle, sys.argv[3])
		
if __name__ == "__main__":
    main_th()

