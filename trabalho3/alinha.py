#coding: utf-8

####################################################
## MC920 1s18 - Trabalho 3                        ##
## Aluna: Elisa Dell'Arriva - ra135551            ##
####################################################


# BIBLIOTECAS
import sys
import trabalho3Projh as ph
import trabalho3Hough as th

def main():
	modo = sys.argv[2]

	if (modo == 'p'):
		print("Método da Projeção horizontal")
		ph.main_ph()
	elif (modo == 'h'):
		print("Método da Transformada de Hough")
		th.main_th()
	else:
		print("Modo inválido. Escolha 'p' para Projeção Horizontal ou 'h' para Transformada de Hough.")
		
if __name__ == "__main__":
    main()
