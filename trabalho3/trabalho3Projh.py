#coding: utf-8

##################################################
## MC920 1s18 - Trabalho 3: Projeção horizontal ##
## Aluna: Elisa Dell'Arriva - ra135551          ##
##################################################


# BIBLIOTECAS
import sys
import numpy as np
import scipy as sp
import matplotlib.pyplot as plot
import matplotlib.image as mpimg
from skimage import filter as skif	#scikit-image 0.10.x
from skimage import color as skic
from skimage import morphology as skim
from skimage import segmentation as skis
from skimage import transform as skit

#FUNÇÕES
#faz a leitura da imagem
def leitura_imagem():
	img_entrada_name = sys.argv[1]
	img = mpimg.imread(img_entrada_name)
	img = skic.rgb2gray(img)	#transforma em escala de cinza

	return img

#realiza o pré processamento na imagem
def preprocessamento(imgb):
	#aplicação do filtro gaussiano
	imgb = skif.gaussian_filter(imgb, sigma=0.6)

	#cast da imagem para inteiro de 8 bits
	imgb = np.trunc(imgb*255)
	imgb = np.uint8(imgb)

	#separação do fundo da imagem
	th = skif.threshold_otsu(imgb) #threshold da binarização
	mask = imgb > th
	imgb[mask] = 255
	mask = imgb <= th
	imgb[mask] = 0

	return imgb

#função auxiliar ao algoritmo de projeção horizontal
def fc_objetivo(a):
	diff = np.diff(a)
	sqrd = np.power(diff, 2)
	value = np.sum(sqrd)

	return value

#realiza a projeção horizontal da imagem
def projecao_horizontal(imgb):
	imgn = 255 - imgb	#obtensão do negativo da imagem	

	perfil = []
	valor = []
	angle = []
	for a in range(0, 180, 5):
		temp = skit.rotate(imgn, a)
		for i in range(0, temp.shape[0]):
			perfil.append(np.sum(temp[i])/255)	#soma dos pixels pretos de cada linha
		valor.append(fc_objetivo(perfil))
		angle.append(a)
		perfil.clear()

	max_idx = np.argmax(valor)

	return angle[max_idx]

#rotaciona imagem original
def rotacao_imagem(img, angle, out_path):
	imgr = skit.rotate(img, angle)
	
	mpimg.imsave(out_path, imgr)
	plot.imshow(imgr, cmap = plot.get_cmap('gray'))
	plot.title("PH: Imagem original rotacionada")
	plot.show()

#função principal
def main_ph():
	img = leitura_imagem()
	imgb = preprocessamento(img)
	
	#plot da imagem após o pré-processamento
	plot.imshow(imgb, cmap = plot.get_cmap('gray'))
	plot.title("PH: Imagem após pré-processamento")
	plot.show()

	best_angle = projecao_horizontal(imgb)
	print("Ângulo: ", best_angle)
	rotacao_imagem(img, best_angle, sys.argv[3])
		
if __name__ == "__main__":
    main_ph()

