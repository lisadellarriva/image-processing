#coding: utf-8

##########################################
## MC920 1s18 - Trabalho 2: Codificação ##
## Aluna: Elisa Dell'Arriva - ra135551  ##
##########################################


# BIBLIOTECAS
import sys
import numpy as np
import scipy as sp
import matplotlib.pyplot as plot
import matplotlib.image as mpimg


########################
## FUNÇÕES AUXILIARES ##
########################

def setar_mascara_AND(plano_bits):
	retorno = int(b'11111110', 2)
	
	if (plano_bits == 1):
		retorno -= 1	#máscara 0b'11111101'
	elif (plano_bits == 2):
		retorno -= 3	#máscara 0b'11111011'

	return retorno

def setar_mascara_OR(plano_bits):
	retorno = int(b'00000001', 2)
	
	if (plano_bits == 1):
		retorno += 1	#máscara 0b'00000010'
	elif (plano_bits == 2):
		retorno += 3	#máscara 0b'00000100'

	return retorno

def setar_mascara_vetor():
	BIT8 = int(b'10000000', 2)
	BIT7 = int(b'01000000', 2)
	BIT6 = int(b'00100000', 2)
	BIT5 = int(b'00010000', 2)
	BIT4 = int(b'00001000', 2)
	BIT3 = int(b'00000100', 2)
	BIT2 = int(b'00000010', 2)
	BIT1 = int(b'00000001', 2)

	return [BIT1, BIT2, BIT3, BIT4, BIT5, BIT6, BIT7, BIT8]


######################################################################################
## CODIFICAÇÃO 																		##
## uso: python3 trabalho2-codifica.py img_entrada.png msg.txt [0|1|2] img_saida.png ##
######################################################################################

img_entrada_name = sys.argv[1]  #imagem onde esconder a mensagem
img_fantasia_name = sys.argv[4] #imagem final contendo mensagem escondida
mensagem_name = sys.argv[2]		#mensagem a ser escondida
plano_bits = sys.argv[3]   		#plano de bits utilizado na codificação (0, 1 ou 2)

#lê imagem de entrada
img_entrada = mpimg.imread(img_entrada_name)

#calcula o número máximo de bits que podem ser codificados na imagem
img_entrada_shape = img_entrada.shape
img_max_bits = img_entrada_shape[0]*img_entrada_shape[1]*3 

#lê arquivo com a mensagem
mensagem = open(mensagem_name, "rb")
mensagem_data = mensagem.read()

#converte para bytearray e adiciona o caracter EOT(end of text) na mensagem
mensagem_data_ba = bytearray(mensagem_data)
mensagem_data_ba.append(3)

#calcula o número de bits na mensagem
mensagem_num_bits = len(mensagem_data_ba)*8;

#testa se mensagem cabe integralmente na imagem
if (mensagem_num_bits > img_max_bits):
	exit("Mensagem muito grande. Não pode ser escondida na imagem em questão.")

#cria máscaras de bits para manipular a mensagem na imagem
MASK_AND = setar_mascara_AND(plano_bits)
MASK_OR = setar_mascara_OR(plano_bits)
BITS_MASK = setar_mascara_vetor()

#converte imagem original de float para unsigned int (intervalo 0 a 255)
img_bytes = (img_entrada * 255).round().astype(np.uint8)

raw = 0
col = 0
cor = 0
#codifica bit a bit cada byte da mensagem na imagem
for msg_byte in mensagem_data_ba:
	for index_bit in range(7,-1,-1):
		bit = msg_byte & BITS_MASK[index_bit]
		
		#modifica o byte atual da imagem		
		if (bit > 0):
			img_bytes[raw][col][cor] = img_bytes[raw][col][cor] | MASK_OR  #codifica um bit 1
		else:
			img_bytes[raw][col][cor] = img_bytes[raw][col][cor] & MASK_AND #codifica um bit 0
		
		#avança para próximo byte da imagem
		if (cor==2):
			cor = 0
			if (col == img_entrada_shape[1]-1):
				col = 0
				raw += 1
				if (raw == img_entrada_shape[0]):
					print("Imagem cheia!!")
			else:
				col += 1
		else:
			cor += 1

#mostra e salva imagem final com mensagem embutida
plot.imshow(img_bytes)
plot.title("Imagem fantasia com mensagem embutida")
plot.show()
plot.imsave(img_fantasia_name, img_bytes, format='png')
print("Mensagem codificada na imagem com sucesso!")

#fecha arquivo
mensagem.close()

