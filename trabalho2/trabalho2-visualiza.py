#coding: utf-8

###########################################
## MC920 1s18 - Trabalho 2: Visualização ##
## Aluna: Elisa Dell'Arriva - ra135551   ##
###########################################


# BIBLIOTECAS
import sys
import numpy as np
import scipy as sp
import matplotlib.pyplot as plot
import matplotlib.image as mpimg


######################################################################
## VISUALIZAÇÃO 		   			  								##
## uso: python3 trabalho2-visualiza.py img_to_analyse.png [r|g|b]	##
######################################################################

#cria máscaras de bits para manipular a mensagem na imagem
BIT8 = int(b'10000000', 2)
BIT3 = int(b'00000100', 2)
BIT2 = int(b'00000010', 2)
BIT1 = int(b'00000001', 2)

#lê canal escolhido da entrada (r, g ou b)
rgb = sys.argv[2]

#ajusta index ao canal escolhido
if (rgb == 'r'):
	cor = 0
elif (rgb == 'g'):
	cor = 1
else:
	cor = 2

#lê imagem da entrada
img_file = sys.argv[1]
img = mpimg.imread(img_file)

#obtém dimensão da imagem
img_shape = img.shape
raw = img_shape[0]
collumn = img_shape[1]

#converte imagem original de float para unsigned int (intervalo 0 a 255)
img_int = (img * 255).round().astype(np.uint8)

#cria um ndarray para cada plano de bits (7, 2, 1, 0)
img_plano7 = np.empty([raw, collumn])
img_plano2 = np.empty([raw, collumn])
img_plano1 = np.empty([raw, collumn])
img_plano0 = np.empty([raw, collumn])

#percorre a imagem toda obtendo os planos de bits
for r in range(0, raw):
	for c in range(0, collumn):
		#plano de bits 0		
		bit0 = img_int[r][c][cor] & BIT1
		if (bit0 > 0):
			#bit no plano 0 é 1
			img_plano0[r][c] = 255
		else:
			#bitno plano 0 é 0
			img_plano0[r][c] = 0
		
		#plano de bits 1
		bit1 = img_int[r][c][cor] & BIT2
		if (bit1 > 0):
			#bit no plano 0 é 1
			img_plano1[r][c] = 255
		else:
			#bitno plano 0 é 0
			img_plano1[r][c] = 0

		#plano de bits 2
		bit2 = img_int[r][c][cor] & BIT3
		if (bit2 > 0):
			#bit no plano 0 é 1
			img_plano2[r][c] = 255
		else:
			#bitno plano 0 é 0
			img_plano2[r][c] = 0

		#plano de bits 7
		bit7 = img_int[r][c][cor] & BIT8
		if (bit1 > 0):
			#bit no plano 0 é 1
			img_plano7[r][c] = 255
		else:
			#bitno plano 0 é 0
			img_plano7[r][c] = 0

#converte as imagens dos planos de bits para unsigned int8
img_plano0 = img_plano0.astype(np.uint8)
img_plano1 = img_plano1.astype(np.uint8)
img_plano2 = img_plano2.astype(np.uint8)
img_plano7 = img_plano7.astype(np.uint8)

#mostra e salva imagens de cada plano de bits
plot.imshow(img_plano7, cmap='gray')
plot.title("Imagem - Plano de bits 7")
plot.show()
plot.imsave("plano_de_bits_7.png", img_plano7)

plot.imshow(img_plano2, cmap='gray')
plot.title("Imagem - Plano de bits 2")
plot.show()
plot.imsave("plano_de_bits_2.png", img_plano2)

plot.imshow(img_plano1, cmap='gray')
plot.title("Imagem - Plano de bits 1")
plot.show()
plot.imsave("plano_de_bits_1.png", img_plano1)

plot.imshow(img_plano0, cmap='gray')
plot.title("Imagem - Plano de bits 0")
plot.show()
plot.imsave("plano_de_bits_0.png", img_plano0)

