#coding: utf-8

############################################
## MC920 1s18 - Trabalho 2: Decodificação ##
## Aluna: Elisa Dell'Arriva - ra135551    ##
############################################


# BIBLIOTECAS
import sys
import numpy as np
import scipy as sp
import matplotlib.pyplot as plot
import matplotlib.image as mpimg

# FUNÇÃO AUXILIAR
def setar_mascara_bit(plano_bits):
	'Gera mascara de bits apropriada para o plano de bits'

	mask = int(b'00000001', 2)
	if (plano_bits == 1):
		mask = 2
	elif (plano_bits == 2):
		mask = 4
	
	return mask


####################################################################################
## DECODIFICAÇÃO 								  ##
## uso: python3 trabalho2-decodifica.py img_com_msg.png [0|1|2] arquivo_saida.txt ##
####################################################################################
#TODO imlpementar possibilidade de qualquer plano de bits

#lê parâmetros da entrada
plano_bits = sys.argv[2]
img_file = sys.argv[1]
text_file = sys.argv[3]

#abre arquivo texto em modo escrita e binário
text = open(text_file, "w")

#abre imagem e obtém sua dimensão
img = mpimg.imread(img_file)
img_shape = img.shape

#converte imagem de float para unsigned int8
img_int = (img * 255).round().astype(np.uint8)

#cria máscaras de bits para manipular a mensagem na imagem
GET_BIT = setar_mascara_bit(plano_bits)

#seta variáveis de controle
byte = 0
END_OF_MSG = False
raw = 0
col = 0
cor = 0
#itera sobre a imagem recuperando bits de mensagem
while (END_OF_MSG == False):
	for index_bit in range(7,-1,-1):
		bit = 0
		#recupera último bit		
		bit = img_int[raw][col][cor] & GET_BIT

		#desloca para esquerda para ajustar ordem de grandeza
		bit = bit << (index_bit)

		#soma valor do bit extraído para compor 1 byte da msg
		byte += bit
	
		#avança para próximo byte da imagem
		if (cor==2):
			cor = 0
			if (col == img_shape[1]-1):
				col = 0
				raw += 1
				if (raw == img_shape[0]):
					print("Imagem vazia!!")
			else:
				col += 1
		else:
			cor += 1
	#fim do for

	#verifica se o byte lido é o fim da mensagem (caracter EOT)
	if (byte == 3):
		#mensagem acabou. terminar
		END_OF_MSG = True;
	else:
		#escrever byte no arquivo e continuar extraindo
		text.write(chr(byte))
		
		#reseta variável byte
		byte = 0
	#fim do while

#fecha arquivo
text.close()

print("Mensagem decodificada com sucesso!")

