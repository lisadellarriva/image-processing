#coding: utf-8
#scikit-image package version 0.10.x

## Aluna: Elisa Dell'Arriva - ra135551 ##

import sys
import numpy as np
import scipy as sp
import matplotlib.pyplot as plot
import matplotlib.image as mpimg
from skimage.color import rgb2gray
import skimage.filter as skf
import skimage.measure as skm
import skimage.segmentation as sks


## ITEM 1.1 TRANFORMAÇÃO DE CORES ##
img_file = sys.argv[1]		#lê imagem passada como argumento e a salva em formato de array numpy
img = mpimg.imread(img_file)

plot.imshow(img) #plota imagem original (colorida)
plot.title("Imagem original")
plot.show()

cinza = rgb2gray(img)	#transforma imagem original em tons de cinza com média ponderada das cores
cinza = np.trunc(cinza*255)	#adequa range para 0 a 255
cinza_int = np.int_(cinza)	#faz cast para inteiro

plot.imshow(cinza, cmap = plot.get_cmap('gray')) #plota imagem em tons de cinza
plot.title("Imagem em tons de cinza")
plot.show()


## ITEM 1.2 CONTORNOS DOS OBJETOS ##
edges = skf.prewitt(cinza) #aplica filtro para marcar as bordas dos objetos

edges_neg = 255 - edges #calcula o negativo da imagem de contornos
plot.imshow(edges_neg, cmap = plot.get_cmap('gray')) #plota imagem com os contornos
plot.title("Contorno dos objetos")
plot.show()


## ITEM 1.3 EXTRAÇÃO DE PROPRIEDADES DOS OBJETOS ##
cinza_int_neg = 255 - cinza_int	
labeled, nlabels = sp.ndimage.measurements.label(cinza_int_neg) #identifica os objetos (componentes conexos)
objetos = skm.regionprops(labeled) #calcula propriedades para cada objeto idetificado
print("número de regiões: ", nlabels)

#seta configurações da imagem com os objetos rotulados
plot.imshow(img, cmap = plot.get_cmap('gray'), shape=img.shape)
plot.title("Objetos rotulados")

area_hist = []
#imprime as propriedades e anota label de cada objeto
for obj in objetos:
	plot.annotate(str(obj.label), xy=(obj.centroid[1], obj.centroid[0])) #rotula objeto na imagem
	print("região: ", obj.label, "área: ", obj.filled_area, "perímetro: ", obj.perimeter) #imprime area e perimetro
	area_hist.extend([obj.filled_area]) 	#armazena areas dos objetos

plot.show() #plota imagem rotulada


## ITEM 1.4 HISTOGRAMA DE ÁREA DOS OBJETOS ##
plot.hist(area_hist, bins=[0,1500,3000,4500], rwidth=0.9) #bins definida conforme intervalos de classificação
plot.title("Histograma das areas dos objetos")
plot.xlabel("area (pequeno: <1500, médio: >=1500 e <3000, grande: >=3000)")
plot.ylabel("numero de objetos")
plot.show() #plota histograma
